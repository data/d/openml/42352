# OpenML dataset: UCI-student-performance-mat

https://www.openml.org/d/42352

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: P. Cortez and A. Silva  
**Source**: [original](http://archive.ics.uci.edu/ml/datasets/Student+Performance) - 2008  
**Please cite**: P. Cortez and A. Silva. Using Data Mining to Predict Secondary School Student Performance. In A. Brito and J. Teixeira Eds., Proceedings of 5th FUture BUsiness TEChnology Conference (FUBUTEC 2008) pp. 5-12, Porto, Portugal, April, 2008, EUROSIS, ISBN 978-9077381-39-7.  

This data approach student achievement in secondary education of two Portuguese schools. The data attributes include student grades, demographic, social and school related features) and it was collected by using school reports and questionnaires.

Two datasets are provided regarding the performance in two distinct subjects: Mathematics (mat) and Portuguese language (por). In [Cortez and Silva, 2008], the two datasets were modeled under binary/five-level classification and regression tasks. This dataset regard the performance in Mathematics.

Important note: the target attribute G3 has a strong correlation with attributes G2 and G1. This occurs because G3 is the final year grade (issued at the 3rd period), while G1 and G2 correspond to the 1st and 2nd period grades. It is more difficult to predict G3 without G2 and G1, but such prediction is much more useful (see paper source for more details).

**Attributes**   
1 school - student's school (binary: 'GP' - Gabriel Pereira or 'MS' - Mousinho da Silveira).  
2 sex - student's sex (binary: 'F' - female or 'M' - male)   
3 age - student's age (numeric: from 15 to 22)   
4 address - student's home address type (binary: 'U' - urban or 'R' - rural)   
5 famsize - family size (binary: 'LE3' - less or equal to 3 or 'GT3' - greater than 3)   
6 Pstatus - parent's cohabitation status (binary: 'T' - living together or 'A' - apart)   
7 Medu - mother's education (numeric: 0 - none, 1 - primary education (4th grade), 2 â€“ 5th to 9th grade, 3 â€“ secondary education or 4 â€“ higher education)   
8 Fedu - father's education (numeric: 0 - none, 1 - primary education (4th grade), 2 â€“ 5th to 9th grade, 3 â€“ secondary education or 4 â€“ higher education)   
9 Mjob - mother's job (nominal: 'teacher', 'health' care related, civil 'services' (e.g. administrative or police), 'at_home' or 'other')   
10 Fjob - father's job (nominal: 'teacher', 'health' care related, civil 'services' (e.g. administrative or police), 'at_home' or 'other')   
11 reason - reason to choose this school (nominal: close to 'home', school 'reputation', 'course' preference or 'other')    
12 guardian - student's guardian (nominal: 'mother', 'father' or 'other')   
13 traveltime - home to school travel time (numeric: 1 - <15>1 hour)   
14 studytime - weekly study time (numeric: 1 - <2>10 hours)   
15 failures - number of past class failures (numeric: n if 1&lt;=n&lt;3, else 4)   
16 schoolsup - extra educational support (binary: yes or no)   
17 famsup - family educational support (binary: yes or no)   
18 paid - extra paid classes within the course subject (Math or Portuguese) (binary: yes or no)   
19 activities - extra-curricular activities (binary: yes or no)   
20 nursery - attended nursery school (binary: yes or no)   
21 higher - wants to take higher education (binary: yes or no)    
22 internet - Internet access at home (binary: yes or no)   
23 romantic - with a romantic relationship (binary: yes or no)   
24 famrel - quality of family relationships (numeric: from 1 - very bad to 5 - excellent)   
25 freetime - free time after school (numeric: from 1 - very low to 5 - very high)   
26 goout - going out with friends (numeric: from 1 - very low to 5 - very high)   
27 Dalc - workday alcohol consumption (numeric: from 1 - very low to 5 - very high)   
28 Walc - weekend alcohol consumption (numeric: from 1 - very low to 5 - very high)   
29 health - current health status (numeric: from 1 - very bad to 5 - very good)   
30 absences - number of school absences (numeric: from 0 to 93)   

these grades are related with the course subject, Math or Portuguese:    
31 G1 - first period grade (numeric: from 0 to 20)   
31 G2 - second period grade (numeric: from 0 to 20)   
32 G3 - final grade (numeric: from 0 to 20, output target)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42352) of an [OpenML dataset](https://www.openml.org/d/42352). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42352/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42352/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42352/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

